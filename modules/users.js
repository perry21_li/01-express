const mongoose = require('mongoose');

mongoose.connect('mongodb://admin:test@localhost:27888/users?authSource=admin', { useNewUrlParser: true, useUnifiedTopology: true });

// define a schema
const userSchema = new mongoose.Schema({ id: String, name: String, age: Number, sex: String });
const User = mongoose.model('User', userSchema);

async function findUsers() {
    console.log('Try to find all users.');
    return await User.find({});
}

function addUser(id, name, age, sex) {
    let user = new User({ id, name, age, sex });
    user.save((err, user) => {
        if (err) {
            console.error(`failed to save user. ${err}`)
            return
        }
        console.log(`user ${id} is saved.`);
    })
}

async function delUser(id) {
    await User.deleteOne({ id })
}

module.exports = { findUsers, addUser, delUser };