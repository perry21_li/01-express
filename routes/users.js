const { findUsers, addUser, delUser } = require('../modules/users');
const express = require('express');
const { deleteModel } = require('mongoose');
const router = express.Router();

/* GET users listing. */
router.get('/', async function(req, res, next) {
    res.send(await findUsers());
});

router.post('/', function(req, res, next) {
    console.log(req.body);
    addUser(req.body.id, req.body.name, req.body.age, req.body.sex);
    res.send(`user ${req.body.id} is added.`);
});

router.delete('/:id', function(req, res, next) {
    let id = req.params.id
    console.log(id);
    delUser(id);
    res.send(`user ${id} is deleted.`);
});

module.exports = router;